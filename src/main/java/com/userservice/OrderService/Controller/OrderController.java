package com.userservice.OrderService.Controller;

import com.userservice.OrderService.DTO.OrderDTO;
import com.userservice.OrderService.Services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    public OrderService orderService;

    //localhost:8083/api/order/getOrdersByUserId/1

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId (@PathVariable final Long id){
        return orderService.getOrdersByUserId(id);
    }

}
