package com.userservice.OrderService.Repositories;

import com.sun.org.apache.bcel.internal.generic.Select;
import com.userservice.OrderService.Entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository <OrderEntity,Long> {
    @Query("SELECT o FROM OrderEntity o WHERE o.userId = ?1")
    List<OrderEntity>findOrdersByUserId(String id);


}
