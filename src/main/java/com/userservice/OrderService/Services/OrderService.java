package com.userservice.OrderService.Services;

import com.userservice.OrderService.DTO.OrderDTO;
import com.userservice.OrderService.Repositories.OrderRepository;
import org.hibernate.criterion.Order;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service

public class OrderService {
    private static final Logger LOGGER= (Logger) LoggerFactory.getLogger(OrderService.class);
    @Autowired
    private OrderRepository repository;

    /**
     * =========================================
     * This method is used to retrieve all user data.
     * =========================================
     * @return
     */

    public List<OrderDTO> getOrdersByUserId(Long id) {
        LOGGER.info("/================ENTER INFO getOrderService in OrderService==============/");
        List<OrderDTO> orders = null;
        try {
            orders = repository.findOrdersByUserId(id.toString())
                    .stream()
                    .map(order -> new OrderDTO(
                            order.getId().toString(),
                            order.getOrderId(),
                            order.getUserId()
                    )).collect(Collectors.toList());
        } catch (Exception e) {LOGGER.warning("/***************Expectation in OrderService ->getAllUsers()*/"+ e);}
        return orders;
    }
}
